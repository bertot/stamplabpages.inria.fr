# stamp.gitlabpages.inria.fr

Sources for the web site stamp.liberabaci.gitlabpages.inria.fr


## Documentation on how to modify the web-site

This web-site is organized around the following process:
files stored in some directories (among which the directory `src`)
   are copied to the web-site every time a user pushes a new version
   on the repository at `git@gitlab.inria.fr:stamp/stamp.gitlabpages.inria.fr`.

### Steps to follow to add your own page

 - To modify the web-site, you need to use `git clone` to get a copy of this
  repository on your working machine.  You can then simply add
  files in the `src` directory, or add your own directory for your files.

 - If you only add a new file to the directory `src`, then `git add`,
   `git commit` and `git push` your file will appear on the web-site,
   but it will not be visible on the
   web-site [main page](https://stamp.gitlabpages.inria.fr).

 - If you add your own directory, you need to modify the file
   `.gitlab-ci.yml` to make sure the contents of that directory is
   mirrored in the `public` directory.  For instance, the current
   setup uses an extra directory `trajectories` and the whole contents
   of that directory is added to the web-site (the contents of the
   web-site is the `public` directory) using a plain `cp` command.

 - This web-site has a main page visible as `index.html`.  If you wish
   your new files to be visible from this main page, you need to
   modify this page accordingly.

 - If you add new files in your own directory, make sure that their
   name is distinct from any other file already present on the
   web-site, otherwise there will be an ambiguity and there is no
   guarantee your file will appear or replace an other file that was
   pre-existing on the web-site.

 - Commit and push to
   `git@gitlab.inria.fr/stamp/stamp.gitlabpages.inria.fr` (`main`
   branch, as indicated in the `.gitlab-ci.yml` file).  When the push
   is performed, the pages are updated, thanks to the directive given
   in the file `.gitlab-ci.yml`

## How this git repository is turned into a web site

Information for people who wish to reproduce this web-site for their
preferred project.

 * This repository needs to be in a group named **NAME**
  (here **NAME** is `stamp`)
 * This repository needs to be named **NAME**`.gitlabpages.inria.fr` (here `stamp.gitlabpages.inria.fr`)
 * The parameters of the repository need to be fine-tuned so that CI is
   activated. This is provided by the section
   `Visibility, project features, permissions` of the `General` Settings.
 * In the `CI/CD` section of the `General` Settings, one must also expand the
  `Runners` section and enable the use of shared runners for this project.
 * The website that is created is `https://stamp.gitlabpages.inria.fr'
 * If you activate CI on a project at location `git@gitlab.inria.fr:othername/stamp.inria.fr.git", then a web-site is created, but with the following address: `https://othername.gitlabpages.inria.fr/stamp.gitlabpages.inria.fr/`

   This gives you a chance to have a *test* web-site (see for example [https://othername.gitlabpages.inria.fr/stamp.gitlabpages.inria.fr](https://othername.gitlabpages.inria.fr/stamp.gitlabpages.inria.fr).